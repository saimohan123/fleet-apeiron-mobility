import React,{useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Divider from '@material-ui/core/Divider';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import pin from '../../assets/pin.svg';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import cancel from '../../assets/cancel.svg';
import details from '../../assets/details.svg';
import Support from '../map/Support'
import Axios from 'axios';
import SimpleModal from './Modal'



const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: "#FBFBFB",
    border: '1px solid #707070',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    width:"837px",
    height:"328px"
  },
  root: {
    '& > *': {
      margin: theme.spacing(2),
      width: '376px',
      height:"38px",
      
    },
    formControl: {
      margin: theme.spacing(1),
      minWidth: 120,
    },
    selectEmpty: {
      marginTop: theme.spacing(2),
    },
    button: {
      margin: theme.spacing(1),
    }   
}
}));

export default function AddDevices() {
  const classes = useStyles();
  const [open, setOpen] = useState(true);  
  const [id,setId]=useState('');
  const [name,setName]=useState('');
  const [type,setType]=useState('');
  const [show,setShow]=useState(false);
  const [data,setData]=useState([])

  let addDeviceHandler=(e)=>{
  e.preventDefault();
  let data={
    "device_id":id,
    "device_name":name,
    "vehicel_type":null
  }
  console.log(data,"data")
  let token=localStorage.getItem("token");
  if(token){
    Axios.get("https://apeiron-mobility.herokuapp.com/create-devices/",data,
    {headers:{'Authorization':`Token ${token}`}})
    .then(resp=>{
      console.log(resp.data)
      setShow(true);
      setOpen(false)
      setName(resp.data[0].device_name)
      setId(resp.data[0].device_id)
      setData(resp.data)
    })
    .catch(error=>{
      console.log(error)
      alert(error)
    })
  }         
  }
  return (
    <div>   
       <Support/>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        // onClose={handleClose}      
        
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <div>
               <img src={pin} />
               <span style={{color:"#1A67A3",fontFamily:"Roboto",fontSize:"20px",fontWeight:"bold",marginLeft:"15px"}}>Add Device</span>
            </div>
           <Divider></Divider>
            <div>
            <form className={classes.root}>      
              <TextField id="outlined-basic" label="Device Id" variant="outlined" name="id" onChange={e=>setId(e.target.value)} />
              <TextField id="outlined-basic" label="Device Name" variant="outlined" name="name" onChange={e=>setName(e.target.value)}/>
              <FormControl variant="outlined" className={classes.formControl}>
        <InputLabel htmlFor="outlined-age-native-simple">vehicle Type</InputLabel>
        <Select
          native  
          onChange={e=>setType(e.target.value)}        
          label="vehicle Type"
          className={classes.option}  
          inputProps={{
            name: 'type',
            id: 'outlined-age-native-simple',
          }}
        >
          <option aria-label="None" value="" />
          <option value="">two</option>
          <option value="">four</option>
          <option value="">six</option>
        </Select>
      </FormControl>     
          </form>
            </div>            
            <div style={{textAlign:"right",marginTop:"60px"}}>
            <div style={{textAlign:"left"}}>
              <img src={details} width="20px" height="20px"/>
              <span style={{letterSpacing:"0.47px",color:"#6C6C6C",font:"Regular 17px Roboto",margin:"5px"}}>Please enter your device details</span>
            </div>
            <Button
        variant="contained"        
        className={classes.button}      
        style={{width:"98px",height:"40px",borderRadius:"5px",backgroundColor:"#707070",color:"#FFFFFF",textTransform:"capitalize",marginRight:"15px"}}
        onClick={e=>setOpen(false)}  
      >
        {/* <img src={cancel} width="16px" height="16px"/> */}
        cancel
      </Button>
      {/* This Button uses a Font Icon, see the installation instructions in the Icon component docs. */}
      <Button
        variant="contained"        
        className={classes.button}  
        style={{width:"120px",height:"40px",borderRadius:"5px",backgroundColor:"#18749D",color:"#FFFFFF",textTransform:"capitalize",marginRight:"15px"}}  
        onClick={addDeviceHandler}
      >
        Add Device       
      </Button>  
            </div>
          </div>
        </Fade>
      </Modal>
     <SimpleModal val={show} name={name} id={id} />

    </div>
  );
}
