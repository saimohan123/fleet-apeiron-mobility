import React, { useState } from "react";
import {
  Grid,
  Paper,
  TableContainer,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TablePagination,
  withStyles,
} from "@material-ui/core";
import { MoreVert } from "@material-ui/icons";
import styles from './Userdetails.style';
import Header from '../layouts/Header'

function ViewSales(props) {
  const { classes } = props;
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [rows, setRows] = React.useState([]);
  const [rowCount, setRowCount] = useState("");

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };
  return (
    <div>
        <Header/>   
      <div style={{ marginTop: "25px ", PaddingBottom: "20px" }}>
        <Grid container spacing={0}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Paper className={classes.root} style={{width:"1000px",height:"500px"}}>
              <TableContainer className={classes.container}>
                <Table stickyHeader aria-label="sticky table">
                  <TableHead>
                    <TableRow>
                      <TableCell className={classes.label}>S.No </TableCell>
                      <TableCell className={classes.label}>
                        Customer Name
                      </TableCell>
                      <TableCell className={classes.label}>
                        Phone Number
                      </TableCell>
                      <TableCell className={classes.label}> product</TableCell>
                      <TableCell className={classes.label}>
                        Shipping Destination
                      </TableCell>
                      <TableCell className={classes.label}>
                        Order Status
                      </TableCell>
                      <TableCell className={classes.label}>
                        Payment Mode
                      </TableCell>
                      <TableCell className={classes.label}>
                        Payment Status
                      </TableCell>
                      <TableCell className={classes.label}></TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {/* {rows
                      .slice(
                        page * rowsPerPage,
                        page * rowsPerPage + rowsPerPage
                      )

                      .map((row, index) => { */}
                    {/* let slno = rowsPerPage * page;

                        return ( */}
                    <TableRow>
                      <TableCell
                        className={classes.rowData}
                        classes={{ root: classes.rootpadding }}
                      >
                        {/* {slno + index + 1} */}01
                      </TableCell>
                      <TableCell className={classes.rowData}>
                        John smith
                      </TableCell>
                      <TableCell className={classes.rowData}>
                        9123456780
                      </TableCell>
                      <TableCell className={classes.rowData}>60mm </TableCell>
                      <TableCell className={classes.rowData}>
                        Hyderabad
                      </TableCell>
                      <TableCell className={classes.rowData}>Pending</TableCell>
                      <TableCell className={classes.rowData}>Cash</TableCell>
                      <TableCell className={classes.rowData}>Pending</TableCell>
                      <TableCell className={classes.rowData}>
                        <MoreVert />
                      </TableCell>
                    </TableRow>
                    {/* ); })} */}
                  </TableBody>
                </Table>
              </TableContainer>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25, 50]}
                component="div"
                count={rowCount}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
              />
            </Paper>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}

export default withStyles(styles)(ViewSales);