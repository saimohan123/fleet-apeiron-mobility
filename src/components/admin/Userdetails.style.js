const styles = {
    space: {
      display: "flex",
      marginRight: "25px",
      marginTop: "20px",
    },
    title: {
      fontSize: " 25px",
      fontFamily: " Roboto medium",
      color: "#000000",
      opacity: 1,
    },
    Button_submit: {
      backgroundColor: "#18749D",
      fontSize: "14px",
      color: "#fff",
      textTransform: "capitalize",
      fontFamily: "roboto regular",
      marginRight: "20px",
      height: "32px !important",
      "& img": { marginRight: "8px" },
      "& p": {},
    },
    formControl: {
      minWidth: 60,
      width: "150px",
      marginRight: "25px",
      height: "30px",
    },
    textField: {
      fontFamily: " 'Roboto', sans-serif",
      backgroundColor: "#ffffff",
      marginBottom: "16px",
  
      "& input": {
        paddingLeft: "8px",
        textOverflow: "ellipsis",
      },
    },
    list_gridicons: {
      width: "28px",
      height: "30px",
      marginRight: "20px",
    },
    cancel: {
      background: " #828282 ",
      borderRadius: "5px",
      opacity: 1,
      padding: "7px",
    },
    firmcard: {
      padding: "0px 0px 20px 22px ",
      backgroundVolor: " #FFFFFF",
      border: " 1px solid #707070",
      borderRadius: "5px",
      opacity: 1,
      width: "93%",
      marginTop: "20px",
    },
    img: {
      width: "94px",
      float: "left",
      marginRight: "25px",
    },
    darkfont: {
      fontFamily: "roboto medium",
      fontSize: "22px",
      letterSpacing: "0px",
      color: " #000000",
      opacity: 1,
    },
    lightfont: {
      fontFamily: "roboto regular",
      fontSize: "20px",
      letterSpacing: "0px",
      color: " #000000",
      opacity: 1,
    },
    /////////////////////////////////view customer list ////////////////////////
    rootpadding: {
      padding: "8px !important",
    },
    container: {
      // maxHeight: 300,
    },
    label: {
      textAlign: "center",
      fontSize: "20px",
      fontFamily: "roboto ",
      fontWeight: "500",
      letterSpacing: "0px",
      color: "#000000",
      textTransform: "capitalize",
      opacity: 1,
    },
    rowData: {
      textAlign: "center",
      fontSize: "20px",
      fontFamily: "roboto regular",
      letterSpacing: "0px",
      color: "#000000",
      opacity: 1,
    },
    titleall: {
      textAlign: "left",
      fontSize: "25px",
      fontFamily: "roboto medium",
      color: "#000000",
      opacity: 1,
    },
    advancehistory: {
      FontFamily: "Roboto Regular",
      fontSize: "16px",
      color: "#00B0FF",
    },
    button: {
      backgroundColor: "#18749D",
      color: "#fff",
      marginTop: "8px",
      textTransform: "capitalize",
    },
  };
  export default styles;