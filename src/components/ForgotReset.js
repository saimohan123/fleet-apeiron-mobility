    import React, { Component } from 'react';
    import { makeStyles } from '@material-ui/core/styles';
    import { Typography, Card,TextInput, TextField, responsiveFontSizes,Button, withStyles, CardMedia } from '@material-ui/core';
    import CardActions from '@material-ui/core/CardActions';
    import CardContent from '@material-ui/core/CardContent';
    import FormControlLabel from '@material-ui/core/FormControlLabel';
    import Checkbox from '@material-ui/core/Checkbox';
    import Grid from '@material-ui/core/Grid';
    import './Forgot1.css';
    import ArrowBackIcon from '@material-ui/icons/ArrowBack';
    import './forgot.css';
    import {FORGOT_RESET} from '../constants';
    import SimpleModal from './PopUp'
    import Axios from 'axios';
    // import styles from './Forgot.style';
        class Forgot1 extends Component {
        constructor(props) {
          super(props)
          this.state = {  
            email:'',password1:'',password2:'',uid:'',token:'',reset:false
          }
        }
      componentDidMount(){
        let email=localStorage.getItem("mail");
        if(email){
          this.setState({email:email})
        };     
        let uid=this.props.match.params.uid;
        let token=this.props.match.params.token;
          this.setState({
            uid:uid,
            token:token
          })
      }
        changeHandler=(e)=>{
        this.setState({
          [e.target.name]:e.target.value
        })
        }
      resetHandler=(e)=>{
        e.preventDefault();
        let data={
          "new_password1":this.state.password1,
          "new_password2":this.state.password2,
          "uid":this.state.uid,
          "token":this.state.token
        }
        Axios.post("https://apeiron-mobility.herokuapp.com/rest-auth/password/reset/confirm/",data)
        .then(resp=>{
          console.log(resp.data);
          this.setState({
            reset:true
          })
        })
        .catch(error=>{
          alert(error)
        })
      }

          render() {
            // const {classes}=this.props
              return (
                  <div className="main">
                    <div class="lg"></div>
    <Grid container>
        <Grid xs={4}></Grid>
        <Grid xs={4}>
          <Card className="fp-card">
          <img
              className="img"
              src={require('../assets/logo.png')}
            />
    <Grid xs={2}>
    <Typography className="pswd1">Set Password</Typography>
    </Grid>
    <Typography className="content">Please enter new password</Typography>
    <Grid xs={12}>
    <TextField className="email-box"  margin="dense" variant="outlined" type="email" name="email" label="registeredmailid@mail.com" value={this.state.email} /> 
    <div className="m-i"></div>
    </Grid>
    <Grid xs={12}>
    <TextField className="psd-box"  margin="dense" variant="outlined" label="Enter Your New Password" type="password" name="password1" value={this.state.password1} onChange={this.changeHandler}  /> 
    <div className="lock-i"></div>
    </Grid>
    <Grid xs={12}>
    <TextField className="psdbox"  helperText="Incorrect entry." margin="dense" variant="outlined" label="Confirm Your New Password" type="email" type="password" name="password2" value={this.state.password2}  onChange={this.changeHandler}  /> 
    <div className="lock-i"></div>
    </Grid>
    <Grid container xs={12}>
    <Button variant="contained" className="rst-1" onClick={this.resetHandler} >
      <p  style={{fontSize:'19px',color:'#FFFFFFDE',marginTop:'0px', textAlign:'center'}}>Reset</p> 
      </Button>
    </Grid>
          </Card>
        </Grid>
        <Grid xs={4}></Grid>
    </Grid>
    <ArrowBackIcon/>
    <Typography className="unknown"> Back to Login</Typography>
    <SimpleModal frgt_reset={this.state.reset}/>
                  </div>
              )
          }
        }
        // export default withStyles(styles) (Forgot);
        export default (Forgot1);