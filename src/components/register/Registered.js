import React,{useState,useEffect} from 'react';
import SimpleModal from '../PopUp';
import {REG_ACTIVE} from '../../constants';
import Axios from 'axios';
import SignIn from '../signin';
import Alert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      '& > * + *': {
        marginTop: theme.spacing(2),
        textAlign:"center"
      },
    },
  }));

function Registered(props) {
    const classes = useStyles();

  
    const [reg_active,setReg_active]=useState(false);
    const [err,setErr]=useState('') 

    useEffect(() => {
         console.log(props.match.params.key);
         let key=props.match.params.key;
         if(key){
             Axios.post(`https://apeiron-mobility.herokuapp.com/registration/account-confirm-email/${key}/`)
             .then(resp=>{
                 console.log(resp.data);
                 setReg_active(true)
             })
             .catch(error=>{
                //  alert(error)
                setErr(error)
             })
         }
    }, [])
    return (
        <div>  
            {err &&
            <div className={classes.root}>
      <Alert severity="error" style={{textAlign:"center"}}>Some thing Went wrogn ,please try again!</Alert>      
            </div>} 
            <SignIn/>       
            <SimpleModal regSuccess={reg_active}/>
        </div>
    )
}

export default Registered