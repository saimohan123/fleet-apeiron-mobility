import React, { Component } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Card,TextInput, TextField, responsiveFontSizes,Button,Divider, CardMedia } from '@material-ui/core';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {Link} from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import './Register.css';
import Axios from 'axios'
import BlockIcon from '@material-ui/icons/Block';
import {REG_URL} from '../../constants';
import SimpleModal from '../PopUp'
// import backgrd './assets/backgrd.png';
const useStyles = makeStyles((theme) => ({
  root: {
    
  },
  card: {
    padding: theme.spacing(3),
    textAlign: 'center',
    color:'red'
  },
  gtext:{
  fontSize:'18',
  fontFamily: " 'Roboto', sans-serif",
  color:'#000000',
  },
  inbox: {
textAlign:'center',
marginLeft:'28',
color:'red',
backgroundColor:'red'
  }
}));
  class Register extends Component {
   
  
  constructor(props) {
    super(props)
    this.state = {
        email:'',password1:'',password2:'',isRegistered:false,error:''
    }
  }  
  handleChange=(e)=>{
    this.setState({
        [e.target.name]:e.target.value
    })
  }
  regHandler=(e)=>{
   e.preventDefault();
   let data={
       "email":this.state.email,
       "password1":this.state.password1,
       "password2":this.state.password2
   }
   console.log(data);
   Axios.post("https://apeiron-mobility.herokuapp.com/rest-auth/registration/",data)
   .then(resp=>{
       console.log(resp.data);
       this.setState({
         isRegistered:true
       })
   })
   .catch(error=>{
       console.log(error)
      //  alert(error)
        if(error=="Error: Request failed with status code 400"){
          this.setState({error:"user is already registered with this e-mail address"})
        }
        if(error=="Error: Network Error"){
          this.setState({error:"check your internet connection"})
        }
   })
  }
    render() {
        return (
            <div className="main">
              <Grid container>
               <Grid item  xs={4} className="red" justify="right" color="red" spacing="3"></Grid>
        <Grid item justify="center" color="green"  xs={4}>
          
        <div>
                     <Card className="card-r"> 
                    <form>
                    <img
          className="image"
          src={require('./logo.png')}
        />
                      <div className="logo">
                      </div>
                      <Typography className="Text-info">Sign up</Typography>
                      <Typography className="smtxt">Get Started</Typography>
                      <TextField 
                      type="email" 
                      name="email" 
                      value={this.state.email}
                      onChange={this.handleChange}
                      variant="outlined" 
                      margin="dense" 
                      label="Enter your email"
                      className="in-box">
                      </TextField>
                      <div className="msg1"></div>
                      <TextField 
                      id="outlined-basic"
                      type="password"  
                      name="password1"
                      value={this.state.password1}
                      onChange={this.handleChange}
                      variant="outlined" 
                      className="box-1"
                      label="Enter your password"
                      margin="dense"
                      >
                      </TextField>
                      <div className="lock"></div>
                      <TextField 
                      id="outlined-basic"
                      type="password"  
                      name="password2"
                      value={this.state.password2}
                      onChange={this.handleChange}
                      variant="outlined" 
                      className="box2"
                      label="Confirm your password"
                      margin="dense"
                      >
                      </TextField>
                      <div className="lock"></div>
                      <div>
                      {/* <BlockIcon className="icon"/> */}
                      <Typography className="icon-txt">{this.state.error}</Typography>
                      <Typography className="icn-t"></Typography>
                      </div>
                      <Button variant="contained" className="button-r" onClick={this.regHandler}>
                      sign up
                      
                      </Button>
                      {/* <Divider className="hrtext" />  */}
                      {/* <b className="bold-r">Or</b> */}
                      <Divider className="hrr" /> 
                      {/* <Button variant="contained" className="gbtn-r">
                      <span className="gtext">Google</span>
                      </Button>
                      <Button variant="contained" className="fbtn-r">
                      facebook
                      </Button>                       */}
                      </form>
                      </Card> 
                      </div>
                      
        </Grid>
        <Grid item color="blue" xs={4}>
        </Grid>
        </Grid>
                   <SimpleModal reg={this.state.isRegistered}/> 
                  </div>
       )
   }
}
export default (Register);