import React from "react";
import { subscribe } from "mqtt-react";
import { divIcon } from "leaflet";
import { renderToStaticMarkup } from "react-dom/server"
import { Map as LeafletMap, TileLayer, Marker, Popup,Polyline } from "react-leaflet";
import 'leaflet-rotatedmarker';
import Axios from "axios";
import bike from '../../assets/bike.png'
import Header from "../layouts/Header";

class Map extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      lat: "12.996041",
      log: "77.697487",
      newdata:[]    
    };
  }
  static getDerivedStateFromProps(nextProps,prevState){
    console.log(nextProps ,"nextProps")
    if(nextProps.data.length>0){
      console.log(nextProps.data,"nextProps");
      let key=nextProps.data;
      let duplicates = key.filter( (ele, ind) => ind === key.findIndex( elem => elem.I === ele.I))   
      if(duplicates!==prevState.newdata){
        return { newdata:duplicates };
        console.log(duplicates,"duplicates")
     }
     else return null;      
        // this.setState({details:duplicates})
      
    }
}

//   vehicleRootHandler=(vehicle_id)=>{
//     let token = localStorage.getItem("token");
//     let dat=moment().format('DD/MM/YYYY')    
//     let data={
//       vehicle:vehicle_id, 
//       date:"20/02/2020" 
//     } 
//     //API call   
//     Axios.post(VEHICLE_ROUTES, data,    
//     {headers: { Authorization: `Token ${token}` }
//   })
//     .then(resp => {
//     console.log(resp.data,"roots"); 
//         if(resp.data){
//           let fill=resp.data.flat().map(i=>[i.La,i.Lo,i.time]);
//           console.log(fill,"fill")
//           this.setState({roots:fill})
//         }
//    } );

//   }

  render() {   
    const { dat, inf, third } = this.state;
    const bikeMarkup = renderToStaticMarkup(<img src={bike} width="30px" />);
    const customMarkerBike = divIcon({
      html: bikeMarkup,
      iconSize: [2,2]      
    });
    // const iconMarkup = renderToStaticMarkup(<img src="car.png" />);
    // const customMarkerIcon = divIcon({
    //   html: iconMarkup
    // });    
    // const truckMarkup = renderToStaticMarkup(<img src="bike.png" width="25px" />);
    // const customMarkertruck = divIcon({      
    //   html: truckMarkup
    // });
    // const idleMarkup = renderToStaticMarkup(<img src="idle.png" />);
    // const customMarkeridle = divIcon({
    //   html: idleMarkup
    // });  
    console.log(this.state.newdata,"newdata")
    return (
      <div>       
        {/* <Header/>       */}
              <LeafletMap
                center={[this.state.lat,this.state.log]}
                draggable={true}
                // position={this.state.lat,this.state.log}
                zoom={13}
                maxZoom={19}
                attributionControl={false}
                zoomControl={true}                
                doubleClickZoom={true}
                scrollWheelZoom={true}
                dragging={true}
                animate={true}
                easeLinearity={0.35}
              >
                <TileLayer url="http://{s}.tile.osm.org/{z}/{x}/{y}.png" />              
                {this.state.newdata &&
                         this.state.newdata.map(i=>(
                           <React.Fragment>
                             <Marker
                               key={1}
                              position={[i.La,i.Lo]} 
                              rotationAngle={270} 
                              rotationOrigin="center"
                               icon={customMarkerBike}>
                              <Popup>
                                <p>{i.I}</p>
                              </Popup>
                            </Marker>  
                           </React.Fragment>
                         ))
                      }                      
              </LeafletMap>            
      </div>
    );
  }
}

export default subscribe({
  topic:"apeiron/default"
})(Map);