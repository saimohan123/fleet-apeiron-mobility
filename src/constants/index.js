export const BASE_URL="https://fleet-management-app.herokuapp.com";
export const LOGIN_URL=`${BASE_URL}/rest-auth/login/`;
export const REG_URL=`${BASE_URL}/rest-auth/registration/`;
export const REG_ACTIVE=`${BASE_URL}/registration/account-confirm-email`;
export const FORGOT_CHECK=`${BASE_URL}/emailcheck/`;
// http://fleet-management-app.herokuapp.com/registration/account-confirm-email/${key}/
export const FORGOT_RESET=`${BASE_URL}/rest-auth/password/reset/confirm/`;