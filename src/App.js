import React from 'react';
import Login from './components/signin';
import Register from './components/register/Register';
import Registered from './components/register/Registered'
import Forgot from'./components/Forgot';
import Forgot1 from './components/ForgotReset'
import './App.css';
import {BrowserRouter,Route,Switch} from 'react-router-dom'
import Support from './components/map/Support';
import Map from './components/map/Map';
import UserDeatails from './components/admin/UserDetails';
import AddDevices from './components/admin/AddDevices'

function App() {
  return (
    <BrowserRouter>
    <div className="App">
      <Switch>
      <Route path="/" exact component={Login}/>    
      <Route path='/register' component={Register}/>
      <Route path="/account-confirm-email/:key/" component={Registered}/>
      <Route path="/forgot" component={Forgot}/>
      <Route path="/password-reset/confirm/:uid/:token/" component={Forgot1}/>
      <Route path="/tracking" component={Support}/>
      <Route path="/userdetails" component={UserDeatails}/>
      <Route path="/adddevices" component={AddDevices}/>
      </Switch>   
    </div>
    </BrowserRouter>
  );
}

export default App;